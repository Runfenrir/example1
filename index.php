<?php

$cacheFile = __DIR__ . '/cache/index.html';
$templateFile = __DIR__ . '/index.html';
$newsFile = __DIR__ . '/news.json';

if (filemtime($newsFile) >= filemtime($cacheFile)) {
    unlink($cacheFile);
}

if (!file_exists($cacheFile)) {
    $template = file_get_contents($templateFile);
    $output = str_replace('{{news}}', renderNewsContent($newsFile), $template);
    file_put_contents($cacheFile, $output);
    $responseCode = 200;
} else {
    $output = file_get_contents($cacheFile);
    $responseCode = 304;
}

header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($cacheFile)) . ' GMT', true, $responseCode);
header('Content-Type: text/html');
echo $output;

function renderNewsContent($newsFile) {
    $output = '';
    $news = json_decode(file_get_contents($newsFile));
    foreach ($news as $singleNews) {
        $output .= '<h2>' . $singleNews->title . '</h2>';
        $output .= '<div>' . $singleNews->teaser . '</div>';
    }
    return $output;
}